var ipc=require('node-ipc');
let minidb = {}
 
ipc.config.id   = 'minidb';
ipc.config.retry= 1500;

ipc.serve(
    () => {
        ipc.server.on(
            'request',
            (data,socket) => {
                const { action, content } = data

                let response = {
                    data: {},
                    message: ''
                }

                switch(action) {
                    case 'CREATE':
                        minidb = Object.assign(minidb, { [`${content.name}`]: content.value })
                        response = {
                            data: minidb,
                            message: `${content.name} successfully CREATED`
                        }
                        break
                    case 'READ':
                        response = {
                            data: minidb,
                            message: `${content.name} successfully READ`
                        }
                        break
                    case 'UPDATE':
                        minidb = {...minidb, [`${content.name}`]: content.value }
                        response = {
                            data: minidb,
                            message: `${content.name} successfully UPDATED`
                        }
                        break
                    case 'DELETE':
                        delete minidb[`${content.name}`]
                        response = {
                            data: minidb,
                            message: `${content.name} successfully DELETED`
                        }
                        break;
                    default:
                        response = {
                            message: 'Unknown action'
                        }
                }

                ipc.server.emit(
                    socket,
                    'response',
                    response
                )
            }
        );
        ipc.server.on(
            'socket.disconnected',
            () => {
                ipc.log('client has disconnected!');
            }
        );
    }
);
 
ipc.server.start();
 
