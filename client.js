var ipc=require('node-ipc');
 
ipc.config.id   = 'client';
ipc.config.retry= 1500;

ipc.connectTo(
    'minidb',
    () => {
        ipc.of.minidb.on(
            'connect',
            () => {
                ipc.of.minidb.emit(
                    'request',
                    {
                        action: 'CREATE',
                        content: {
                            name: 'savings',
                            value: 0
                        }
                    }
                )
                ipc.of.minidb.emit(
                    'request',
                    {
                        action: 'READ',
                        content: {
                            name: 'savings',
                        }
                    }
                )
                ipc.of.minidb.emit(
                    'request',
                    {
                        action: 'UPDATE',
                        content: {
                            name: 'savings',
                            value: 5213612
                        }
                    }
                )
                ipc.of.minidb.emit(
                    'request',
                    {
                        action: 'DELETE',
                        content: {
                            name: 'savings',
                        }
                    }
                )
                ipc.of.minidb.emit(
                    'request',
                    {
                        action: 'READ',
                        content: {
                            name: 'savings',
                        }
                    }
                )
            }
        );
        ipc.of.minidb.on(
            'response',
            (data) => {
                ipc.log('RESPONSE:', data)
            }
        );
        ipc.of.minidb.on(
            'disconnect',
            () => {
                ipc.log('disconnected from minidb');
            }
        );
    }
);
